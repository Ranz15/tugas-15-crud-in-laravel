<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Facade\FlareClient\View;

//                                                      Get Method

// Menampilkan Halaman Table
Route::get('/table', function() {
    return view('table');
});

// Menampilkan Halaman Data Tables
Route::get('/data-tables', function() {
    return view('data-tables');
});

// Menampilkan halaman home
Route::get('/', 'HomeController@index');

// Menampilkan Halaman Register
Route::get('/register', 'AuthController@index');

// Menampilkan Data Cast
Route::get('/cast', 'CastController@index');

// Menampilkan Tampilan Tambah Data Cast
Route::get('/cast/create', 'CastController@create');

// Menampilkan detail data
Route::get('/cast/{cast_id}', 'CastController@show');

// Menampilkan data untuk di rubah
Route::get('/cast/{cast_id}/edit', 'CastController@edit');

//                                                         Post Method
// Menerima dan insert into database
Route::post('/welcome', 'AuthController@welcome');

// Menerima dan insert into database
Route::post('/cast', 'CastController@store');

//                                                         Put Method

// Menerima hasil edit dan insert into database
Route::put('/cast/{cast_id}', 'CastController@update');

//                                                         Delete Method

// Menghapus Data sesuai ID/Request
Route::delete('/cast/{cast_id}', 'CastController@destroy');