@extends('layouts.admin.master')

@section('title')
    Rubah Data Cast
@endsection

@section('content')
    <form action="/cast/{{$casts->id}}" method="post">
        @csrf
        @method('put')
        <div class="form-group">
            <label for="name">Nama</label>
            <input
                type="text"
                class="form-control"
                id="name"
                name="nama"
                value="{{$casts->nama}}"
            />
        </div>
        @error('nama')
        <div class="alert alert-danger">{{ $message }}</div>
        @enderror

        <div class="form-group">
            <label for="age">Umur</label>
            <input
                type="number"
                class="form-control"
                id="age"
                name="umur"
                value="{{$casts->umur}}"
            />
        </div>
        @error('umur')
        <div class="alert alert-danger">{{ $message }}</div>
        @enderror

        <div class="form-group">
            <label for="Biography">Bio</label>
            <textarea
                class="form-control"
                id="Biography"
                rows="3"
                name="bio"
            >{{$casts->bio}}</textarea>
        </div>
        @error('bio')
        <div class="alert alert-danger">{{ $message }}</div>
        @enderror

        <div>
            <button type="submit" class="form-control btn btn-primary">Save</button>
        </div>
    </form>
@endsection