@extends('layouts.admin.master')

@section('title')
    Tambah Data Cast
@endsection

@section('content')
    <form action="/cast" method="post">
        @csrf
        <div class="form-group">
            <label for="name">Nama</label>
            <input
                type="text"
                class="form-control"
                id="name"
                placeholder="isikan nama pemeran"
                name="nama"
            />
        </div>
        @error('nama')
        <div class="alert alert-danger">{{ $message }}</div>
        @enderror

        <div class="form-group">
            <label for="age">Umur</label>
            <input
                type="number"
                class="form-control"
                id="age"
                placeholder="isikan umur pemeran"
                name="umur"
            />
        </div>
        @error('umur')
        <div class="alert alert-danger">{{ $message }}</div>
        @enderror

        <div class="form-group">
            <label for="Biography">Bio</label>
            <textarea
                class="form-control"
                id="Biography"
                rows="3"
                name="bio"
            ></textarea>
        </div>
        @error('bio')
        <div class="alert alert-danger">{{ $message }}</div>
        @enderror

        <div>
            <button type="submit" class="form-control btn btn-primary">Save</button>
        </div>
    </form>
@endsection