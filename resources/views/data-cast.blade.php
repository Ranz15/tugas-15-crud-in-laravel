@extends('layouts.admin.master')

@section('title')
    Data Cast
@endsection

@section('content')

    {{-- Tombol untuk menampilkan halaman tambah data --}}
    <a href="/cast/create" class="btn btn-primary mb-3">Tambah Data</a>
    
    <table class="table">
        <thead class="thead-dark">
            <tr>
                <th scope="col">No</th>
                <th scope="col">Nama</th>
                <th scope="col">Umur</th>
                <th scope="col">Bio</th>
                <th scope="col">Action</th>
            </tr>
        </thead>
        <tbody>
            @forelse ($casts as $cast)
                <tr>
                    <th scope="row">{{$loop->iteration}}</th>
                    <td>{{$cast->nama}}</td>
                    <td>{{$cast->umur}}</td>
                    <td>{{$cast->bio}}</td>
                    <td>
                        <form action="/cast/{{$cast->id}}" method="post">
                            @csrf
                            @method('delete')
                            <a href="/cast/{{$cast->id}}" class="btn btn-primary btn-sm">Show</a>
                            <a href="/cast/{{$cast->id}}/edit" class="btn btn-info btn-sm">Edit</a>  
                            <input type="submit" class="btn btn-danger btn-sm" value="Delete">
                        </form>
                    </td>
                </tr>
            @empty
                <tr>
                    <td>Empty Data</td>
                </tr>
            @endforelse
        </tbody>
    </table>
@endsection